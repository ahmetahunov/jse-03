package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.entity.Task;
import java.util.*;

public class TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    public Collection<Task> getTasks() {
        return tasks.values();
    }

    public Task getTask(String name, String projectId) {
        for (Task task : getTasks()) {
            if (task.getName().equals(name) && task.getProjectId().equals(projectId))
                return task;
        }
        return null;
    }

    public List<Task> getTasksByProjectId(String projectId) {
        List<Task> tasks = new ArrayList<>();
        for (Task task : getTasks()) {
            if (task.getProjectId().equals(projectId)) {
                tasks.add(task);
            }
        }
        return tasks;
    }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(Task task) {
        tasks.remove(task.getId());
    }

    public void clear() {
        tasks.clear();
    }

}
