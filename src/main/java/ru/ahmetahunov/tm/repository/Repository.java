package ru.ahmetahunov.tm.repository;

public class Repository {

    private final TaskRepository tasks = new TaskRepository();

    private final ProjectRepository projects = new ProjectRepository();

    public TaskRepository getTasks() {
        return tasks;
    }

    public ProjectRepository getProjects() {
        return projects;
    }

}
