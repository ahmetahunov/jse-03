package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.entity.Project;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public List<Project> getProjects() {
        return projects;
    }

    public Project getProject(String name) {
        for (Project project : projects) {
            if (project.getName().equals(name))
                return project;
        }
        return null;
    }

    public Project getProjectById(String id) {
        for (Project project : projects) {
            if (project.getId().equals(id))
                return project;
        }
        return null;
    }

    public void add(Project project) {
        projects.add(project);
    }

    public void remove(Project project) {
        projects.remove(project);
    }

    public void clear() {
        projects.clear();
    }

}
