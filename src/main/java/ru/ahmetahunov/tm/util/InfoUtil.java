package ru.ahmetahunov.tm.util;

import ru.ahmetahunov.tm.entity.Item;

public class InfoUtil {

    public static String getInfo(Item item) {
        StringBuilder sb = new StringBuilder();
        sb.append(item.getName());
        sb.append("\n");
        sb.append("Description: ");
        sb.append(item.getDescription());
        sb.append("\n");
        sb.append("Start date: ");
        String date = DateUtil.formatDate(item.getStartDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        sb.append("\n");
        sb.append("Finish date: ");
        date = DateUtil.formatDate(item.getFinishDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        return sb.toString();
    }

}
