package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.repository.TaskRepository;

class ProjectClearCommand implements Command {

    @Override
    public void execute(Repository repository) {
        TaskRepository taskRepository = repository.getTasks();
        ProjectRepository projectRepository = repository.getProjects();
        for (Project project : projectRepository.getProjects()) {
            for (Task task : taskRepository.getTasksByProjectId(project.getId()))
                taskRepository.remove(task);
        }
        projectRepository.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public String toString() {
        return "project-clear: Remove all projects.";
    }

}