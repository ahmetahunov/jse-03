package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.Repository;

class TaskListCommand implements Command {

    @Override
    public void execute(Repository repository) {
        int i = 1;
        System.out.println("[TASK LIST]");
        for (Task task : repository.getTasks().getTasks()) {
            System.out.println(String.format("%d. %s", i++, task.getName()));
        }
    }

    @Override
    public String toString() {
        return "task-list: Show all available tasks.";
    }

}