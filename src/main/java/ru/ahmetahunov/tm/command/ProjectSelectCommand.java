package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import java.io.IOException;
import java.util.List;

public class ProjectSelectCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        ProjectRepository projectRepository = repository.getProjects();
        TaskRepository taskRepository = repository.getTasks();
        System.out.println("[PROJECT SELECT]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = projectRepository.getProject(name);
        if (project == null) {
            System.out.println("Selected project not exists.");
            return;
        }
        System.out.println(project.getName());
        List<Task> taskList = taskRepository.getTasksByProjectId(project.getId());
        if (taskList.size() != 0)
            System.out.println("Task:");
        int i = 1;
        for (Task task : taskList) {
            System.out.println(String.format("  %d. %s", i++, task.getName()));
        }
    }

    @Override
    public String toString() {
        return "project-select: Show selected project with tasks.";
    }

}
