package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.repository.Repository;

class TaskClearCommand implements Command {

    @Override
    public void execute(Repository repository) {
        repository.getTasks().clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public String toString() {
        return "task-clear: Remove all tasks.";
    }

}