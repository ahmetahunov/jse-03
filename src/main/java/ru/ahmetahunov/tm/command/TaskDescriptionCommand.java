package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public class TaskDescriptionCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        System.out.println("[TASK-DESCRIPTION]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage();
        Project project = repository.getProjects().getProject(name);
        if (project == null && !("".equals(name))) {
            System.out.println("Selected project not exists.");
            return;
        }
        System.out.print("Please enter task name: ");
        name = ConsoleHelper.readMessage();
        String projectId = (project == null) ? "" : project.getId();
        Task task = repository.getTasks().getTask(name, projectId);
        if (task == null) {
            System.out.println("Selected task not exists.");
            return;
        }
        System.out.println(InfoUtil.getInfo(task));
    }

    @Override
    public String toString() {
        return "task-description: Show task description.";
    }
}
