package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import java.io.IOException;
import java.util.List;

class ProjectRemoveCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        ProjectRepository projectRepository = repository.getProjects();
        TaskRepository taskRepository = repository.getTasks();
        System.out.println("[PROJECT REMOVE]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = projectRepository.getProject(name);
        if (project == null) {
            System.out.println("Selected project not exists.");
            return;
        }
        projectRepository.remove(project);
        List<Task> taskList = taskRepository.getTasksByProjectId(project.getId());
        for (Task task : taskList) {
            taskRepository.remove(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public String toString() {
        return "project-remove: Remove selected project.";
    }

}