package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.repository.Repository;

class ProjectListCommand implements Command {

    @Override
    public void execute(Repository repository) {
        int i = 1;
        System.out.println("[PROJECT LIST]");
        for (Project project : repository.getProjects().getProjects()) {
            System.out.println(String.format("%d. %s", i++, project.getName()));
        }
    }

    @Override
    public String toString() {
        return "project-list: Show all available projects.";
    }

}