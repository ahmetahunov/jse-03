package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.repository.Repository;
import java.io.IOException;

interface Command {

    void execute(Repository repository) throws IOException;

}