package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import java.io.IOException;

class TaskRemoveCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        ProjectRepository projectRepository = repository.getProjects();
        TaskRepository taskRepository = repository.getTasks();
        System.out.println("[TASK REMOVE]");
        System.out.print("Enter project name or press enter to skip:");
        String name = ConsoleHelper.readMessage().trim();
        Project project = null;
        String projectId = null;
        if (!"".equals(name)) {
            project = projectRepository.getProject(name);
            if (project == null) {
                System.out.println("Selected project not exists");
                System.out.println("[OPERATION CANCELLED]");
                return;
            }
            projectId = project.getId();
        }
        System.out.print("Enter task name: ");
        name = ConsoleHelper.readMessage().trim();
        Task task = taskRepository.getTask(name, projectId);
        if (task != null) {
            taskRepository.remove(task);
            System.out.println("[OK]");
            return;
        }
        System.out.println("Selected task not exists.");
    }

    @Override
    public String toString() {
        return "task-remove: Remove selected task.";
    }

}