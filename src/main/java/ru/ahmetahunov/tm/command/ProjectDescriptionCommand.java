package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public class ProjectDescriptionCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        System.out.println("[PROJECT-DESCRIPTION]");
        System.out.print("Please enter project name: ");
        Project project = repository.getProjects().getProject(ConsoleHelper.readMessage());
        if (project == null) {
            System.out.println("Selected project not exists.");
            return;
        }
        System.out.println(InfoUtil.getInfo(project));
    }

    @Override
    public String toString() {
        return "project-description: Show project information.";
    }
}
