package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.enumerated.Operation;
import ru.ahmetahunov.tm.repository.Repository;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class CommandExecutor {

    private final Map<Operation, Command> commands;

    private final Repository repository = new Repository();

    public CommandExecutor() {
        commands = new TreeMap<>();
        commands.put(Operation.HELP, new HelpCommand(commands));
        commands.put(Operation.PROJECT_CLEAR, new ProjectClearCommand());
        commands.put(Operation.PROJECT_CREATE, new ProjectCreateCommand());
        commands.put(Operation.PROJECT_LIST, new ProjectListCommand());
        commands.put(Operation.PROJECT_REMOVE, new ProjectRemoveCommand());
        commands.put(Operation.PROJECT_SELECT, new ProjectSelectCommand());
        commands.put(Operation.PROJECT_DESCRIPTION, new ProjectDescriptionCommand());
        commands.put(Operation.TASK_CLEAR, new TaskClearCommand());
        commands.put(Operation.TASK_CREATE, new TaskCreateCommand());
        commands.put(Operation.TASK_REMOVE, new TaskRemoveCommand());
        commands.put(Operation.TASK_LIST, new TaskListCommand());
        commands.put(Operation.TASK_DESCRIPTION, new TaskDescriptionCommand());
        commands.put(Operation.TASK_MOVE, new TaskMoveCommand());
        commands.put(Operation.EXIT, new ExitCommand());
    }

    public void execute(Operation operation) throws IOException {
        if (operation == Operation.UNKNOWN) {
            System.out.println("Unknown operation." +
                    " Please enter correct command or 'help' to list available operations.");
            return;
        }
        Command command = commands.get(operation);
        command.execute(repository);
    }

}