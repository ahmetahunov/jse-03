package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

class TaskCreateCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        TaskRepository taskRepository = repository.getTasks();
        System.out.println("[TASK CREATE]");
        System.out.print("Enter project name or press enter to skip: ");
        Project project = getProject(repository);
        System.out.print("Please enter task name: ");
        String name = ConsoleHelper.readMessage().trim();
        Task task = taskRepository.getTask(name, (project == null) ? null : project.getId());
        if (task != null) {
            System.out.println(name + " already exists.");
            if (!isReplacedTask(task, taskRepository))
                return;
        }
        task = createNewTask(name, project);
        taskRepository.add(task);
        System.out.println("[OK]");
    }

    private Task createNewTask(String name, Project project) throws IOException {
        Task task = new Task();
        task.setName(name);
        System.out.print("Please enter description: ");
        task.setDescription(ConsoleHelper.readMessage());
        System.out.print("Please enter start date(example: 01.01.2020): ");
        task.setStartDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        task.setFinishDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        if (project != null) {
            task.setProjectId(project.getId());
        }
        return task;
    }

    private Project getProject(Repository repository) throws IOException {
        ProjectRepository projectRepository = repository.getProjects();
        String name = ConsoleHelper.readMessage().trim();
        if (!("".equals(name))) {
            Project project = projectRepository.getProject(name);
            if (project != null)
                return project;
            String answer = null;
            System.out.println(name + " is not available.");
            do {
                System.out.println("Do you want use another project?<y/n>");
                answer = ConsoleHelper.readMessage();
                if ("y".equals(answer))
                    return getProject(repository);
            } while (!("n".equals(answer)));
        }
        return null;
    }

    private boolean isReplacedTask(Task task, TaskRepository tasks) throws IOException {
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                return false;
            }
        }while (!("y".equals(answer)));
        tasks.remove(task);
        return true;
    }

    @Override
    public String toString() {
        return "task-create: Create new task.";
    }

}