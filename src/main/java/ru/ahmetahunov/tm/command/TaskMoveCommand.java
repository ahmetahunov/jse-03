package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.Repository;
import java.io.IOException;

public class TaskMoveCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        ProjectRepository projects = repository.getProjects();
        System.out.println("[TASK-MOVE]");
        System.out.print("Please enter project name: ");
        String projectName = ConsoleHelper.readMessage();
        Project project = projects.getProject(projectName);
        System.out.print("Please enter task name: ");
        String taskName = ConsoleHelper.readMessage();
        String projectId = "";
        if (project != null)
            projectId = project.getId();
        Task task = repository.getTasks().getTask(taskName, projectId);
        if (task == null) {
            System.out.println("Selected task not exists.");
            return;
        }
        System.out.print("Please enter new project name: ");
        project = projects.getProject(ConsoleHelper.readMessage());
        if (project == null)
            task.setProjectId("");
        else
            task.setProjectId(project.getId());
        System.out.println("[OK]");
    }

    @Override
    public String toString() {
        return "task-move: Change project for task.";
    }

}
