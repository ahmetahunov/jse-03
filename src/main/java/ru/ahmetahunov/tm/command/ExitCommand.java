package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.repository.Repository;

class ExitCommand implements Command {

    public void execute(Repository repository) {
        System.out.println("Have a nice day!");
    }

    public String toString() {
        return "exit: exit from task manager";
    }

}