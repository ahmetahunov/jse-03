package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.Repository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;
import java.util.List;

class ProjectCreateCommand implements Command {

    @Override
    public void execute(Repository repository) throws IOException {
        ProjectRepository projectRepository = repository.getProjects();
        System.out.println("[PROJECT CREATE]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = projectRepository.getProject(name);
        if (project != null) {
            System.out.println(name + " already exists.");
            if (!isReplacedProject(project, projectRepository, repository.getTasks()))
                return;
        }
        project = createNewProject(name);
        projectRepository.add(project);
        System.out.println("[OK]");
    }

    private Project createNewProject(String name) throws IOException {
        Project project = new Project();
        project.setName(name);
        System.out.println("Please enter description:");
        project.setDescription(ConsoleHelper.readMessage());
        System.out.print("Please insert start date(example: 01.01.2020): ");
        project.setStartDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        project.setFinishDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        return project;
    }

    private boolean isReplacedProject(Project project, ProjectRepository projectRepository, TaskRepository taskRepository)
            throws IOException {
        String answer = null;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                return false;
            }
        } while (!("y".equals(answer)));
        projectRepository.remove(project);
        List<Task> taskList = taskRepository.getTasksByProjectId(project.getId());
        for (Task task : taskList) {
            taskRepository.remove(task);
        }
        return true;
    }

    @Override
    public String toString() {
        return "project-create: Create new project.";
    }

}