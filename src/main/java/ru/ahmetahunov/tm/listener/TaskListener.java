package ru.ahmetahunov.tm.listener;

import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.enumerated.Operation;
import ru.ahmetahunov.tm.command.CommandExecutor;
import java.io.IOException;

public class TaskListener {

    private final CommandExecutor executor;

    private final String HELP = "help";

    private final String EXIT = "exit";

    private final String PROJECT_CLEAR = "project-clear";

    private final String PROJECT_CREATE = "project-create";

    private final String PROJECT_LIST = "project-list";

    private final String PROJECT_REMOVE = "project-remove";

    private final String PROJECT_DESCRIPTION = "project-description";

    private final String PROJECT_SELECT = "select-project";

    private final String TASK_CLEAR = "task-clear";

    private final String TASK_CREATE = "task-create";

    private final String TASK_LIST = "task-list";

    private final String TASK_REMOVE = "task-remove";

    private final String TASK_DESCRIPTION = "task-description";

    private final String TASK_MOVE = "task-move";

    public TaskListener(CommandExecutor executor) {
        this.executor = executor;
    }

    public void listen() throws IOException {
        while (true) {
            System.out.print("Please enter command: ");
            Operation operation = getOperation(ConsoleHelper.readMessage());
            executor.execute(operation);
            if (operation == Operation.EXIT) {
                ConsoleHelper.close();
                break;
            }
            System.out.println();
        }
    }

    private Operation getOperation(String command) {
        command = command.trim().toLowerCase();
        switch (command) {
            case (HELP) : return Operation.HELP;
            case (PROJECT_CLEAR) : return Operation.PROJECT_CLEAR;
            case (PROJECT_CREATE) : return Operation.PROJECT_CREATE;
            case (PROJECT_LIST) : return Operation.PROJECT_LIST;
            case (PROJECT_REMOVE) : return Operation.PROJECT_REMOVE;
            case (PROJECT_SELECT) : return Operation.PROJECT_SELECT;
            case (PROJECT_DESCRIPTION) : return Operation.PROJECT_DESCRIPTION;
            case (TASK_CLEAR) : return Operation.TASK_CLEAR;
            case (TASK_CREATE) : return Operation.TASK_CREATE;
            case (TASK_LIST) : return Operation.TASK_LIST;
            case (TASK_REMOVE) : return Operation.TASK_REMOVE;
            case (TASK_DESCRIPTION) : return Operation.TASK_DESCRIPTION;
            case (TASK_MOVE) : return Operation.TASK_MOVE;
            case (EXIT) : return Operation.EXIT;
            default: return Operation.UNKNOWN;
        }
    }

}
