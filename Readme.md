# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven 4.0

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/jse-03.git
cd jse-03
mvn clean install
```

## run app
```bash
java -jar target/jse-1.0.3.jar
```